
package enom151.ecs.edicom.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getConstancia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getConstancia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expediente" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="idExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getConstancia", propOrder = {
    "expediente",
    "idExt"
})
public class GetConstancia {

    protected byte[] expediente;
    protected String idExt;

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setExpediente(byte[] value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExt() {
        return idExt;
    }

    /**
     * Define el valor de la propiedad idExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExt(String value) {
        this.idExt = value;
    }

}
