
package enom151.ecs.edicom.com;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.teknei.bidserver.edicom package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckStatus_QNAME = new QName("http://com.edicom.ecs.enom151", "checkStatus");
    private final static QName _GetConstanciaHashResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaHashResponse");
    private final static QName _VerifyConstanciaResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaResponse");
    private final static QName _GetConstanciaAsPdf_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaAsPdf");
    private final static QName _GetConstanciaAsPdfResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaAsPdfResponse");
    private final static QName _ECSException_QNAME = new QName("http://com.edicom.ecs.enom151", "ECSException");
    private final static QName _GetConstanciaResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaResponse");
    private final static QName _RefrendaConstanciaResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "refrendaConstanciaResponse");
    private final static QName _VerifyConstancia_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstancia");
    private final static QName _CheckStatusResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "checkStatusResponse");
    private final static QName _VerifyConstanciaFile_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaFile");
    private final static QName _VerifyConstanciaXml_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaXml");
    private final static QName _GetConstancia_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstancia");
    private final static QName _GetConstanciaFile_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaFile");
    private final static QName _VerifyConstanciaXmlResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaXmlResponse");
    private final static QName _RefrendaConstancia_QNAME = new QName("http://com.edicom.ecs.enom151", "refrendaConstancia");
    private final static QName _VerifyConstanciaPdf_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaPdf");
    private final static QName _GetConstanciaFileResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaFileResponse");
    private final static QName _VerifyConstanciaFileResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaFileResponse");
    private final static QName _VerifyConstanciaPdfResponse_QNAME = new QName("http://com.edicom.ecs.enom151", "verifyConstanciaPdfResponse");
    private final static QName _GetConstanciaHash_QNAME = new QName("http://com.edicom.ecs.enom151", "getConstanciaHash");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.teknei.bidserver.edicom
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VerifyConstanciaXmlResponse }
     * 
     */
    public VerifyConstanciaXmlResponse createVerifyConstanciaXmlResponse() {
        return new VerifyConstanciaXmlResponse();
    }

    /**
     * Create an instance of {@link VerifyConstanciaPdf }
     * 
     */
    public VerifyConstanciaPdf createVerifyConstanciaPdf() {
        return new VerifyConstanciaPdf();
    }

    /**
     * Create an instance of {@link RefrendaConstancia }
     * 
     */
    public RefrendaConstancia createRefrendaConstancia() {
        return new RefrendaConstancia();
    }

    /**
     * Create an instance of {@link VerifyConstanciaFileResponse }
     * 
     */
    public VerifyConstanciaFileResponse createVerifyConstanciaFileResponse() {
        return new VerifyConstanciaFileResponse();
    }

    /**
     * Create an instance of {@link GetConstanciaFileResponse }
     * 
     */
    public GetConstanciaFileResponse createGetConstanciaFileResponse() {
        return new GetConstanciaFileResponse();
    }

    /**
     * Create an instance of {@link GetConstanciaHash }
     * 
     */
    public GetConstanciaHash createGetConstanciaHash() {
        return new GetConstanciaHash();
    }

    /**
     * Create an instance of {@link VerifyConstanciaPdfResponse }
     * 
     */
    public VerifyConstanciaPdfResponse createVerifyConstanciaPdfResponse() {
        return new VerifyConstanciaPdfResponse();
    }

    /**
     * Create an instance of {@link GetConstanciaAsPdf }
     * 
     */
    public GetConstanciaAsPdf createGetConstanciaAsPdf() {
        return new GetConstanciaAsPdf();
    }

    /**
     * Create an instance of {@link GetConstanciaAsPdfResponse }
     * 
     */
    public GetConstanciaAsPdfResponse createGetConstanciaAsPdfResponse() {
        return new GetConstanciaAsPdfResponse();
    }

    /**
     * Create an instance of {@link VerifyConstanciaResponse }
     * 
     */
    public VerifyConstanciaResponse createVerifyConstanciaResponse() {
        return new VerifyConstanciaResponse();
    }

    /**
     * Create an instance of {@link CheckStatus }
     * 
     */
    public CheckStatus createCheckStatus() {
        return new CheckStatus();
    }

    /**
     * Create an instance of {@link GetConstanciaHashResponse }
     * 
     */
    public GetConstanciaHashResponse createGetConstanciaHashResponse() {
        return new GetConstanciaHashResponse();
    }

    /**
     * Create an instance of {@link GetConstanciaResponse }
     * 
     */
    public GetConstanciaResponse createGetConstanciaResponse() {
        return new GetConstanciaResponse();
    }

    /**
     * Create an instance of {@link ECSException }
     * 
     */
    public ECSException createECSException() {
        return new ECSException();
    }

    /**
     * Create an instance of {@link VerifyConstanciaXml }
     * 
     */
    public VerifyConstanciaXml createVerifyConstanciaXml() {
        return new VerifyConstanciaXml();
    }

    /**
     * Create an instance of {@link CheckStatusResponse }
     * 
     */
    public CheckStatusResponse createCheckStatusResponse() {
        return new CheckStatusResponse();
    }

    /**
     * Create an instance of {@link VerifyConstanciaFile }
     * 
     */
    public VerifyConstanciaFile createVerifyConstanciaFile() {
        return new VerifyConstanciaFile();
    }

    /**
     * Create an instance of {@link VerifyConstancia }
     * 
     */
    public VerifyConstancia createVerifyConstancia() {
        return new VerifyConstancia();
    }

    /**
     * Create an instance of {@link RefrendaConstanciaResponse }
     * 
     */
    public RefrendaConstanciaResponse createRefrendaConstanciaResponse() {
        return new RefrendaConstanciaResponse();
    }

    /**
     * Create an instance of {@link GetConstancia }
     * 
     */
    public GetConstancia createGetConstancia() {
        return new GetConstancia();
    }

    /**
     * Create an instance of {@link GetConstanciaFile }
     * 
     */
    public GetConstanciaFile createGetConstanciaFile() {
        return new GetConstanciaFile();
    }

    /**
     * Create an instance of {@link RestConstanciaResult }
     * 
     */
    public RestConstanciaResult createRestConstanciaResult() {
        return new RestConstanciaResult();
    }

    /**
     * Create an instance of {@link EcsConstanciaResult }
     * 
     */
    public EcsConstanciaResult createEcsConstanciaResult() {
        return new EcsConstanciaResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "checkStatus")
    public JAXBElement<CheckStatus> createCheckStatus(CheckStatus value) {
        return new JAXBElement<CheckStatus>(_CheckStatus_QNAME, CheckStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaHashResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaHashResponse")
    public JAXBElement<GetConstanciaHashResponse> createGetConstanciaHashResponse(GetConstanciaHashResponse value) {
        return new JAXBElement<GetConstanciaHashResponse>(_GetConstanciaHashResponse_QNAME, GetConstanciaHashResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaResponse")
    public JAXBElement<VerifyConstanciaResponse> createVerifyConstanciaResponse(VerifyConstanciaResponse value) {
        return new JAXBElement<VerifyConstanciaResponse>(_VerifyConstanciaResponse_QNAME, VerifyConstanciaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaAsPdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaAsPdf")
    public JAXBElement<GetConstanciaAsPdf> createGetConstanciaAsPdf(GetConstanciaAsPdf value) {
        return new JAXBElement<GetConstanciaAsPdf>(_GetConstanciaAsPdf_QNAME, GetConstanciaAsPdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaAsPdfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaAsPdfResponse")
    public JAXBElement<GetConstanciaAsPdfResponse> createGetConstanciaAsPdfResponse(GetConstanciaAsPdfResponse value) {
        return new JAXBElement<GetConstanciaAsPdfResponse>(_GetConstanciaAsPdfResponse_QNAME, GetConstanciaAsPdfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ECSException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "ECSException")
    public JAXBElement<ECSException> createECSException(ECSException value) {
        return new JAXBElement<ECSException>(_ECSException_QNAME, ECSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaResponse")
    public JAXBElement<GetConstanciaResponse> createGetConstanciaResponse(GetConstanciaResponse value) {
        return new JAXBElement<GetConstanciaResponse>(_GetConstanciaResponse_QNAME, GetConstanciaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefrendaConstanciaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "refrendaConstanciaResponse")
    public JAXBElement<RefrendaConstanciaResponse> createRefrendaConstanciaResponse(RefrendaConstanciaResponse value) {
        return new JAXBElement<RefrendaConstanciaResponse>(_RefrendaConstanciaResponse_QNAME, RefrendaConstanciaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstancia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstancia")
    public JAXBElement<VerifyConstancia> createVerifyConstancia(VerifyConstancia value) {
        return new JAXBElement<VerifyConstancia>(_VerifyConstancia_QNAME, VerifyConstancia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "checkStatusResponse")
    public JAXBElement<CheckStatusResponse> createCheckStatusResponse(CheckStatusResponse value) {
        return new JAXBElement<CheckStatusResponse>(_CheckStatusResponse_QNAME, CheckStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaFile")
    public JAXBElement<VerifyConstanciaFile> createVerifyConstanciaFile(VerifyConstanciaFile value) {
        return new JAXBElement<VerifyConstanciaFile>(_VerifyConstanciaFile_QNAME, VerifyConstanciaFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaXml")
    public JAXBElement<VerifyConstanciaXml> createVerifyConstanciaXml(VerifyConstanciaXml value) {
        return new JAXBElement<VerifyConstanciaXml>(_VerifyConstanciaXml_QNAME, VerifyConstanciaXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstancia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstancia")
    public JAXBElement<GetConstancia> createGetConstancia(GetConstancia value) {
        return new JAXBElement<GetConstancia>(_GetConstancia_QNAME, GetConstancia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaFile")
    public JAXBElement<GetConstanciaFile> createGetConstanciaFile(GetConstanciaFile value) {
        return new JAXBElement<GetConstanciaFile>(_GetConstanciaFile_QNAME, GetConstanciaFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaXmlResponse")
    public JAXBElement<VerifyConstanciaXmlResponse> createVerifyConstanciaXmlResponse(VerifyConstanciaXmlResponse value) {
        return new JAXBElement<VerifyConstanciaXmlResponse>(_VerifyConstanciaXmlResponse_QNAME, VerifyConstanciaXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefrendaConstancia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "refrendaConstancia")
    public JAXBElement<RefrendaConstancia> createRefrendaConstancia(RefrendaConstancia value) {
        return new JAXBElement<RefrendaConstancia>(_RefrendaConstancia_QNAME, RefrendaConstancia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaPdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaPdf")
    public JAXBElement<VerifyConstanciaPdf> createVerifyConstanciaPdf(VerifyConstanciaPdf value) {
        return new JAXBElement<VerifyConstanciaPdf>(_VerifyConstanciaPdf_QNAME, VerifyConstanciaPdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaFileResponse")
    public JAXBElement<GetConstanciaFileResponse> createGetConstanciaFileResponse(GetConstanciaFileResponse value) {
        return new JAXBElement<GetConstanciaFileResponse>(_GetConstanciaFileResponse_QNAME, GetConstanciaFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaFileResponse")
    public JAXBElement<VerifyConstanciaFileResponse> createVerifyConstanciaFileResponse(VerifyConstanciaFileResponse value) {
        return new JAXBElement<VerifyConstanciaFileResponse>(_VerifyConstanciaFileResponse_QNAME, VerifyConstanciaFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyConstanciaPdfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "verifyConstanciaPdfResponse")
    public JAXBElement<VerifyConstanciaPdfResponse> createVerifyConstanciaPdfResponse(VerifyConstanciaPdfResponse value) {
        return new JAXBElement<VerifyConstanciaPdfResponse>(_VerifyConstanciaPdfResponse_QNAME, VerifyConstanciaPdfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConstanciaHash }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://com.edicom.ecs.enom151", name = "getConstanciaHash")
    public JAXBElement<GetConstanciaHash> createGetConstanciaHash(GetConstanciaHash value) {
        return new JAXBElement<GetConstanciaHash>(_GetConstanciaHash_QNAME, GetConstanciaHash.class, null, value);
    }

}
