
package enom151.ecs.edicom.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getConstanciaHash complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getConstanciaHash">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hashExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getConstanciaHash", propOrder = {
    "hashExpediente",
    "idExt"
})
public class GetConstanciaHash {

    protected String hashExpediente;
    protected String idExt;

    /**
     * Obtiene el valor de la propiedad hashExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHashExpediente() {
        return hashExpediente;
    }

    /**
     * Define el valor de la propiedad hashExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHashExpediente(String value) {
        this.hashExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExt() {
        return idExt;
    }

    /**
     * Define el valor de la propiedad idExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExt(String value) {
        this.idExt = value;
    }

}
