
package enom151.ecs.edicom.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para verifyConstancia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="verifyConstancia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="constancia" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="hash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyConstancia", propOrder = {
    "constancia",
    "hash",
    "idExt"
})
public class VerifyConstancia {

    protected byte[] constancia;
    protected String hash;
    protected String idExt;

    /**
     * Obtiene el valor de la propiedad constancia.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getConstancia() {
        return constancia;
    }

    /**
     * Define el valor de la propiedad constancia.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setConstancia(byte[] value) {
        this.constancia = value;
    }

    /**
     * Obtiene el valor de la propiedad hash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHash() {
        return hash;
    }

    /**
     * Define el valor de la propiedad hash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHash(String value) {
        this.hash = value;
    }

    /**
     * Obtiene el valor de la propiedad idExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExt() {
        return idExt;
    }

    /**
     * Define el valor de la propiedad idExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExt(String value) {
        this.idExt = value;
    }

}
