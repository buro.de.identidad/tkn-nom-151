package com.teknei.edicom.ecs.nom151;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;

import com.teknei.edicom.ecs.nom151.client.Enom151Exception;
import com.teknei.edicom.ecs.nom151.helper.ConstanciaData;
import com.teknei.edicom.ecs.nom151.helper.FileUtils;
import com.teknei.edicom.ecs.nom151.helper.GestorLogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Enom151Program
{
	
  public static final String ASN_EXTENSION = ".asn";
  public static final String TSR_EXTENSION = ".tsr";
  public static final String ALL_ASN_FILES = "*.asn";
  public static final String ASN_STATUS = ".status";
  public static final String ALL_FILES = "*";
  public static final String BLANK = "";
  public static final String GET_CONSTANCIA = "GetConstancia";
  public static final String GET_CONSTANCIA_TSR = "GetConstanciaTSR";
  public static final String CHECK_STATUS = "CheckStatus";
  public static final String REFRENDA_CONSTANCIA = "RefrendaConstancia";
  public static final String VERIFY_CONSTANCIA = "VerifyConstancia";
  public static final String VERIFY_CONSTANCIA_TSR = "VerifyConstanciaTSR";
  public static final String LOAD_PROPERTIES = "Load Properties";
  public static final String INITIALIZE_LOGGER = "Initialize Logger";
  public static final String LOG_PROPERTIES_FILE = "enom151Log4j.properties";
  public static final String CLIENT_PROPERTIES_FILE = "enom151Client.properties";
  public static final String PROPERTIES_URL = "url";
  public static final char PROPERTIES_SHORT_URL = 'l';
  public static final String PROPERTIES_PFX = "pfx";
  public static final char PROPERTIES_SHORT_PFX = 's';
  public static final String PROPERTIES_KEY = "key";
  public static final char PROPERTIES_SHORT_KEY = 'k';
  public static final String PROPERTIES_USER = "user";
  public static final char PROPERTIES_SHORT_USER = 'u';
  public static final String PROPERTIES_PASS = "pass";
  public static final char PROPERTIES_SHORT_PASS = 'p';
  public static final String PROPERTIES_DOMAIN = "domain";
  public static final char PROPERTIES_SHORT_DOMAIN = 'd';
  public static final String PROPERTIES_GROUP = "group";
  public static final char PROPERTIES_SHORT_GROUP = 'g';
  public static final String PROPERTIES_DIR_IN = "in";
  public static final char PROPERTIES_SHORT_DIR_IN = 'i';
  public static final String PROPERTIES_DIR_OUT = "out";
  public static final char PROPERTIES_SHORT_DIR_OUT = 'o';
  public static final String PROPERTIES_CONSTANCIA = "constancia";
  public static final char PROPERTIES_SHORT_CONSTANCIA = 'c';
  public static final String PROPERTIES_FILE = "file";
  public static final char PROPERTIES_SHORT_FILE = 'f';
  public static final String PROPERTIES_HASH = "hash";
  public static final char PROPERTIES_SHORT_HASH = 't';
  public static final String PROPERTIES_IDEXT = "idExt";
  public static final char PROPERTIES_SHORT_IDEXT = 'e';
  public static final String PROPERTIES_CHECKTYPE = "checkType";
  public static final char PROPERTIES_SHORT_CHECKTYPE = 'q';
  public static final String PROPERTIES_HELP = "help";
  public static final char PROPERTIES_SHORT_HELP = 'h';
  public static final String PROPERTIES_DELETE_INPUT_FILES = "deleteInputFiles";
  public static final char PROPERTIES_SHORT_DELETE_INPUT_FILES = 'r';
  private Properties properties;
  
  public Enom151Program()
    throws Enom151Exception
  {
    initializeLogger();
    
    loadProperties();
    
    inicializaProperties();
  }
  



  protected Properties getProperties()
  {
    return this.properties;
  }
  


  private void initializeLogger()
    throws Enom151Exception
  {
    Properties logProperties = new Properties();
    try {
      File f = new File(getLogPropertiesFile());
      logProperties.load(new FileInputStream(f));
      PropertyConfigurator.configure(logProperties);
      GestorLogs.info(this, "Initialize Logger", "Logging inicializado: " + getLogPropertiesFile() + ".");
    } catch (IOException ioException) {
      String error = "initializeLogger: Error al cargar el fichero del log de la aplicacion: " + getLogPropertiesFile() + ".";
      GestorLogs.info(this, "Initialize Logger", error, ioException);
      throw new Enom151Exception(1, error);
    } catch (Exception exception) {
      String error = "initializeLogger: Error al inicializar el log de la aplicacion: " + getLogPropertiesFile() + ".";
      GestorLogs.info(this, "Initialize Logger", error, exception);
      throw new Enom151Exception(1, error);
    }
  }
  


  private void loadProperties()
    throws Enom151Exception
  {
    GestorLogs.info(this, "loadProperties", "start");
    String errorText = "";
    this.properties = new Properties();
    try {
      File f = new File(getClientPropertiesFile());
      getProperties().load(new FileInputStream(f));
    } catch (FileNotFoundException fnfException) {
      errorText = "Fichero de propiedades no encontrado " + getClientPropertiesFile() + ".";
      GestorLogs.error(this, "Load Properties", errorText, fnfException);
      throw new Enom151Exception(2, errorText);
    } catch (IOException ioException) {
      errorText = "No se puede leer el fichero de propiedades " + getClientPropertiesFile() + ".";
      GestorLogs.error(this, "Load Properties", errorText, ioException);
      throw new Enom151Exception(2, errorText);
    } catch (Exception exception) {
      errorText = "Error al leer el fichero de propiedades " + getClientPropertiesFile() + ".";
      GestorLogs.error(this, "Load Properties", errorText, exception);
      throw new Enom151Exception(2, errorText);
    }
    
    GestorLogs.info(this, "Load Properties", "end");
  }
  




  protected void insertaValorProperties(String key, String value)
  {
    if (value != null) {
      getProperties().setProperty(key, value);
    }
  }
  



  public void inicializaProperties()
  {
    GestorLogs.info(this, "Inicializa Properties", "start");
    
    String rutaAplicacion = FileUtils.preparaRutaDirectorio(System.getProperty("user.dir"));
    String in = getProperties().getProperty("in");
    if ((in == null) || (in.equals(""))) {
      getProperties().setProperty("in", rutaAplicacion);
    }
    GestorLogs.info(this, "Inicializa Properties", "end");
  }
  




  protected void insertaValorProperties(String key, Boolean value)
  {
    if (value != null) {
      insertaValorProperties(key, value.toString());
    }
  }
  




  protected boolean verifyProperty(String key)
  {
    boolean ok = getProperties().containsKey(key);
    if (ok) {
      String value = getProperties().getProperty(key);
      ok = (value != null) && (!value.equals(""));
    }
    return ok;
  }
  


  protected boolean verifyProperties()
    throws Enom151Exception
  {
    if (!verifyProperty("user"))
      throw new Enom151Exception(3, "Falta por insertar el usuario que accede al servicio.");
    if (!verifyProperty("pass"))
      throw new Enom151Exception(3, "Falta por insertar la clave del usuario que accede al servicio.");
    if (!verifyProperty("url")) {
      throw new Enom151Exception(3, "Falta por insertar la url del servicio.");
    }
    return !getProperties().containsKey("help");
  }
  
  public String getLogPropertiesFile() {
    return "configuration" + File.separator + "enom151Log4j.properties";
  }
  
  public String getClientPropertiesFile() {
    return "configuration" + File.separator + "enom151Client.properties";
  }
  
  public String generateOutputDir(String outputDir, String pattern) {
    Date now = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
    String time = dateFormat.format(now);
    
    StringBuilder newOutputDir = new StringBuilder();
    if (outputDir.endsWith(File.separator)) {
      newOutputDir.append(outputDir).append(time);
    } else {
      newOutputDir.append(outputDir).append(File.separator).append(time);
    }
    
    File fPattern = new File(pattern);
    if (fPattern.exists()) {
      newOutputDir.append("_").append(fPattern.getName());
    } else {
      newOutputDir.append("_").append(pattern);
    }
    
    String finalOutputDir = newOutputDir.toString();
    File f = new File(finalOutputDir);
    int cursor = 1;
    while (f.exists()) {
      finalOutputDir = newOutputDir.toString() + "_" + cursor;
      f = new File(finalOutputDir);
      cursor++;
    }
    return finalOutputDir + File.separator;
  }
  
  public String recoverFileContent(String filename) throws IOException {
    String result = "";
    File fFilename = new File(filename);
    if (fFilename.exists()) {
      result = org.apache.commons.io.FileUtils.readFileToString(fFilename);
    }
    return result;
  }
  
  public void generatedOutputFromContent(byte[] document, String filename, String extension, String finalOutputDir, String method, Object fromClass) throws IOException
  {
    File fFilename = new File(filename);
    String fileName = filename;
    if (fFilename.exists()) {
      fileName = fFilename.getName();
    }
    String newExtension = extension;
    if (StringUtils.isBlank(extension)) {
      newExtension = "";
    }
    if ((document != null) && (document.length > 0)) {
      org.apache.commons.io.FileUtils.writeByteArrayToFile(new File(finalOutputDir + fileName + newExtension), document);
      GestorLogs.info(fromClass, method, "File generated successfully!");
    } else {
      GestorLogs.error(fromClass, method, Enom151Exception.getTextCode(5));
    }
  }
  
  public boolean deleteInputFiles() {
    String delete = getProperties().getProperty("deleteInputFiles");
    if ((StringUtils.isNotBlank(delete)) && (Boolean.parseBoolean(delete))) {
      String dirIn = getProperties().getProperty("in");
      File f = new File(dirIn);
      return f.delete();
    }
    return false;
  }
  
  public String recoverFilename(String filename, String dirIn) {
    String result = null;
    File f = new File(filename);
    if (f.exists()) {
      result = filename;
    } else {
      String path = dirIn + File.separator + filename;
      f = new File(path);
      if (f.exists()) {
        result = path;
      }
    }
    return result;
  }
  
  public String getFilenameFromConstanciaName(String dirIn, String constanciaName) {
    String result = null;
    if (constanciaName.endsWith(".asn")) {
      String fileName = constanciaName.split(".asn")[0].trim();
      result = recoverFilename(fileName, dirIn);
    }
    return result;
  }
  
  public Map<String, String> processFilesHash(File dirIn) throws IOException {
    Map<String, String> filesBytes = new HashMap<String, String>();
    for (int i = 0; i < dirIn.list().length; i++) {
      String filename = dirIn.list()[i];
      String hash = FileUtils.getSha256Timestamp(FileUtils.leerFichero(dirIn.getAbsolutePath() + File.separator + filename));
      filesBytes.put(filename, hash);
    }
    return filesBytes;
  }
  
  public Map<String, String> processFilesHash(File dirIn, String mask) throws IOException {
    Map<String, String> filesBytes = new HashMap<String, String>();
    if ((dirIn.exists()) && (dirIn.list().length > 0)) {
      String[] filenames = FileUtils.getFiles(dirIn.getAbsolutePath(), mask, false);
      for (int i = 0; i < filenames.length; i++) {
        String filename = filenames[i];
        String hash = FileUtils.getSha256Timestamp(FileUtils.leerFichero(dirIn.getAbsolutePath() + File.separator + filename));
        filesBytes.put(filename, hash);
      }
    }
    return filesBytes;
  }
  
  public Map<String, String> processFilesTsr(File dirIn) throws IOException {
    Map<String, String> filesTsrs = new HashMap<String, String>();
    for (int i = 0; i < dirIn.list().length; i++) {
      String filename = dirIn.list()[i];
      String hash = FileUtils.leerFichero(dirIn.getAbsolutePath() + File.separator + filename);
      filesTsrs.put(filename, hash);
    }
    return filesTsrs;
  }
  
  public Map<String, String> processFilesTsr(File dirIn, String mask) throws IOException {
    Map<String, String> filesBytes = new HashMap<String, String>();
    if ((dirIn.exists()) && (dirIn.list().length > 0)) {
      String[] tsrames = FileUtils.getFiles(dirIn.getAbsolutePath(), mask, false);
      for (int i = 0; i < tsrames.length; i++) {
        String tsrname = tsrames[i];
        String hash = FileUtils.leerFichero(dirIn.getAbsolutePath() + File.separator + tsrname);
        filesBytes.put(tsrname, hash);
      }
    }
    return filesBytes;
  }
  
  public Map<String, byte[]> processFilesByte(File dirIn) throws IOException {
    Map<String, byte[]> filesBytes = new HashMap<String, byte[]>();
    for (int i = 0; i < dirIn.list().length; i++) {
      String filename = dirIn.list()[i];
      Path path = Paths.get(dirIn.getAbsolutePath() + File.separator + filename, new String[0]);
      filesBytes.put(filename, Files.readAllBytes(path));
    }
    return filesBytes;
  }
  
  public Map<String, byte[]> processFilesByte(File dirIn, String mask) throws IOException {
    Map<String, byte[]> filesBytes = new HashMap<String, byte[]>();
    if ((dirIn.exists()) && (dirIn.list().length > 0)) {
      String[] filenames = FileUtils.getFiles(dirIn.getAbsolutePath(), mask, false);
      for (int i = 0; i < filenames.length; i++) {
        String filename = filenames[i];
        Path path = Paths.get(dirIn.getAbsolutePath() + File.separator + filename, new String[0]);
        filesBytes.put(filename, Files.readAllBytes(path));
      }
    }
    return filesBytes;
  }
  
  public String getFilename(String[] filenames) { 
    String filename;
    if (filenames[0].endsWith(".asn")) {
      filename = filenames[1];
    } else {
      filename = filenames[0];
    }
    return filename;
  }
  
  public List<ConstanciaData> getFileBytes(String constanciaName, String dirIn) throws IOException {
    List<ConstanciaData> files = new ArrayList<ConstanciaData>();
    File fConstanciaName = new File(constanciaName);
    if (fConstanciaName.exists()) {
      String fileName = getFilenameFromConstanciaName(dirIn, constanciaName);
      files.add(new ConstanciaData(constanciaName, fileName));
    } else {
      String[] constanciaNames = FileUtils.getFiles(dirIn, constanciaName, false);
      for (int i = 0; i < constanciaNames.length; i++) {
        String fileName = getFilenameFromConstanciaName(dirIn, constanciaNames[i]);
        if (fileName != null) {
          files.add(new ConstanciaData(dirIn + File.separator + constanciaNames[i], fileName));
        }
      }
    }
    return files;
  }
  
  public List<ConstanciaData> getHashFiles(String constanciaName, String dirIn) throws IOException {
    List<ConstanciaData> files = new ArrayList<ConstanciaData>();
    File fConstanciaName = new File(constanciaName);
    if (fConstanciaName.exists()) {
      String fileName = getHashFilenameFromConstanciaName(dirIn, constanciaName);
      files.add(new ConstanciaData(constanciaName, fileName));
    } else {
      String[] constanciaNames = FileUtils.getFiles(dirIn, constanciaName, false);
      for (int i = 0; i < constanciaNames.length; i++) {
        String fileName = getHashFilenameFromConstanciaName(dirIn, constanciaNames[i]);
        if (fileName != null) {
          files.add(new ConstanciaData(dirIn + File.separator + constanciaNames[i], fileName));
        }
      }
    }
    return files;
  }
  
  public String getHashFilenameFromConstanciaName(String dirIn, String constanciaName) {
    String result = null;
    if (constanciaName.endsWith(".asn")) {
      String fileName = constanciaName.split(".asn")[0].trim();
      result = recoverHashFilename(fileName, dirIn);
    }
    return result;
  }
  
  public String recoverHashFilename(String filename, String dirIn) {
    String result = null;
    String newFilename = filename;
    if (!newFilename.endsWith(".tsr")) {
      newFilename = newFilename + ".tsr";
    }
    File f = new File(newFilename);
    if (f.exists()) {
      result = newFilename;
    } else {
      String path = dirIn + File.separator + newFilename;
      f = new File(path);
      if (f.exists()) {
        result = path;
      }
    }
    return result;
  }
  
  public String getHashFileContent(File fDirIn, String filename) throws IOException
  {
    String content = FileUtils.leerFichero(fDirIn.getAbsolutePath() + File.separator + filename);
    if (StringUtils.isBlank(content)) {
      content = FileUtils.leerFichero(filename);
    }
    return content;
  }
  
  abstract void addArguments(String[] paramArrayOfString)
    throws Enom151Exception;
  
  abstract void showHelp();
  
  abstract boolean validaDatos()
    throws Enom151Exception;
}
