package com.teknei.edicom.ecs.nom151.client;
import java.net.URL;

import javax.xml.ws.Service;

import com.teknei.edicom.utils.ws.client.DefaultCxfClient;

import enom151.ecs.edicom.com.EcsConstanciaResult;
import enom151.ecs.edicom.com.WebServiceENom151;
import enom151.ecs.edicom.com.WebServiceENom151ImplService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Enom151Client
  extends DefaultCxfClient
{
  private static final Logger log = LoggerFactory.getLogger(Enom151Client.class);
  public Enom151Client(String url, String user, String pass)
    throws Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".Enom151Client 1");
    super(url, user, pass);
  }
  
  public Enom151Client(String wsUrl, byte[] wsFilePfx, String wsPfxPass) throws Exception {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".dataSource");
    super(wsUrl, null, null, 30, null, wsFilePfx, wsPfxPass);
  }
  
  protected Service createService(URL wsUrl) throws Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".createService");
    return new WebServiceENom151ImplService(); //MCG: Lo toma de archivo local
  }
  
  protected Object getServicePort(Service service) throws Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getServicePort");
    WebServiceENom151ImplService serviceObj = (WebServiceENom151ImplService)service;
    return serviceObj.getWebServiceENom151ImplPort();
  }
  
  private WebServiceENom151 getClient() {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getClient");
    return (WebServiceENom151)this.client;
  }
  
  public boolean checkStatus(int checkType) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".checkStatus");
    boolean data = false;
    try {
      data = getClient().checkStatus(checkType);
    } catch (Exception e) {
      this.logger.error("checkStatus", e);
    }
    return data;
  }
  
  public byte[] refrendaConstancia(byte[] constancia, String hash, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".refrendaConstancia");
    byte[] data = null;
    try {
      data = getClient().refrendaConstancia(constancia, hash, idExt);
    } catch (Exception e) {
      this.logger.error("refrendaConstancia", e);
    }
    return data;
  }
  
  public EcsConstanciaResult verifyConstancia(byte[] constancia, String hash, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".verifyConstancia");
    EcsConstanciaResult data = null;
    try {
      data = getClient().verifyConstancia(constancia, hash, idExt);
    } catch (Exception e) {
      this.logger.error("verifyConstancia", e);
    }
    return data;
  }
  
  public EcsConstanciaResult verifyConstanciaFile(byte[] constancia, byte[] file, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".verifyConstanciaFile");
    EcsConstanciaResult data = null;
    try {
      data = getClient().verifyConstanciaFile(constancia, file, idExt);
    } catch (Exception e) {
      this.logger.error("verifyConstanciaFile", e);
    }
    return data;
  }
  
  public byte[] getConstanciaHash(String hashExpediente, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getConstanciaHash");
    byte[] data = null;
    try {
      data = getClient().getConstanciaHash(hashExpediente, idExt);
    } catch (Exception e) {
      this.logger.error("getConstanciaHash", e);
    }
    return data;
  }
  
  public byte[] getConstancia(byte[] expediente, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getConstancia");
    byte[] data = null;
    try {
      data = getClient().getConstancia(expediente, idExt);
    } catch (Exception e) {
      this.logger.error("getConstancia", e);
    }
    return data;
  }
  
  public byte[] getConstanciaFile(byte[] file, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getConstanciaFile");
    byte[] data = null;
    try {
      data = getClient().getConstanciaFile(file, idExt);
    } catch (Exception e) {
      this.logger.error("getConstanciaFile", e);
    }
    return data;
  }

  public byte[] getConstanciaAsPdf(byte[] file, String idExt) {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getConstanciaAsPdf");
	    byte[] data = null;
	    try {
	      data = getClient().getConstanciaAsPdf(file, idExt);
	    } catch (Exception e) {
	      this.logger.error("getConstanciaAsPdf", e);
	    }
	    return data;
	  }
}
